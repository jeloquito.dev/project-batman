package com.jeqplayground.projectbatman;

import com.jeqplayground.projectbatman.customers.Model.Entity.Customer;
import com.jeqplayground.projectbatman.customers.Service.CustomerService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;

@SpringBootApplication
public class ProjectBatmanApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectBatmanApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(CustomerService customerService) {
		return args -> {
			customerService.save(new Customer(null, "Michael Jordan"));
			customerService.save(new Customer(null, "Stephen Curry"));
			customerService.save(new Customer(null, "Kobe Bryant"));
			customerService.save(new Customer(null, "Lebron James"));
		};
	};
}
