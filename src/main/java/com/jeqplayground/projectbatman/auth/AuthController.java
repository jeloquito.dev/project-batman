package com.jeqplayground.projectbatman.auth;

import com.jeqplayground.projectbatman.dto.API.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @GetMapping("/login")
    public ResponseEntity<Response<String>> login() {
        return new ResponseEntity<>(
                new Response<>(
                        HttpStatus.OK.value(),
                        "Successful Login",
                        "OK"), HttpStatus.OK
        );
    }
}
