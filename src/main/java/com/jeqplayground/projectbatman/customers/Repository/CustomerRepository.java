package com.jeqplayground.projectbatman.customers.Repository;

import com.jeqplayground.projectbatman.customers.Model.Entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

}
