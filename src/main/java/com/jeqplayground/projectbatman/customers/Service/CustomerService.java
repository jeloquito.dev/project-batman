package com.jeqplayground.projectbatman.customers.Service;

import com.jeqplayground.projectbatman.customers.Model.Entity.Customer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CustomerService {

    List<Customer> getCustomers();

    Customer getCustomer(Long id);

    void save(Customer customer);

}
