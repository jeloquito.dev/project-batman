package com.jeqplayground.projectbatman.customers.Service.Impl;

import com.jeqplayground.projectbatman.customers.Model.Entity.Customer;
import com.jeqplayground.projectbatman.customers.Repository.CustomerRepository;
import com.jeqplayground.projectbatman.customers.Service.CustomerService;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class CustomerServiceImpl implements CustomerService {

    private CustomerRepository customerRepository;

    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getCustomers() {
        return this.customerRepository.findAll();
    }

    @Override
    public Customer getCustomer(Long id) {
        return customerRepository.findById(id).orElse(null);
    }

    @Override
    public void save(Customer customer) {
        customerRepository.save(customer);
    }
}
