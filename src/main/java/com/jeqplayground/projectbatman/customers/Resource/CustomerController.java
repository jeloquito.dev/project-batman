package com.jeqplayground.projectbatman.customers.Resource;

import com.jeqplayground.projectbatman.customers.Model.Entity.Customer;
import com.jeqplayground.projectbatman.customers.Service.CustomerService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping
    List<Customer> getCustomers() {
        return this.customerService.getCustomers();
    }

    @GetMapping("/{id}")
    Customer getCustomerById(@PathVariable("id") Long id) {
        return this.customerService.getCustomer(id);
    }
}
